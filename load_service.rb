
module LoadService
	def initialize(data)
		super(data)
		@data = data
	end

	private
	def init
		@data.each do |key,value|
			next  unless key.to_s.end_with?('_url')
			next  unless  value
			method_name = key.to_s.gsub('_url','').to_sym
			send(:define_method,method_name) do
				load_data(value)
			end
		end
	end


	def load_data(url)
		uri = URI.parse url
		res = Net::HTTP.get uri
		JSON.parse(res, symbolize_names: true)
	end
end


module StaticLoadService

	def create(data)
		s = new(data)
		s.init
		s
	end
end