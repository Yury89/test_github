require 'net/http'
require 'json'
require 'awesome_print'
require_relative './subscription'
require_relative './organization'
require_relative './repos'
require_relative './load_service'

GET_USER_INFO = ->(username) {  "https://api.github.com/users/#{username}" }.freeze
FOLLOWERS_URL = :followers_url.freeze
SUBSCRIPTIONS_URL = :subscriptions_url.freeze
ORGANIZATION_URL = :organizations_url.freeze
REPO_URL = :repos_url.freeze

def load_data(url)
  uri = URI.parse url
  res = Net::HTTP.get uri
  JSON.parse(res, symbolize_names: true)
end

class User < OpenStruct

  def self.from_username username
   new load_data GET_USER_INFO.call(username)
  end

  def followers
    @followers ||=
        load_data((send FOLLOWERS_URL))
            .map! { |d| User.new d}
  end

  def subscriptions
    @subscriptions ||=
        load_data((send SUBSCRIPTIONS_URL))
          .map{|s| Subscription.create(s)}
  end

  def organizations
    @organizations ||=
        load_data((send ORGANIZATION_URL))
          .map{|s| Organization.new(s)}
  end

  def repos
    @repos ||=
        load_data((send REPO_URL))
            .map{|s| Repos.new(s)}
  end
end

user = User.from_username('yuriGithub')
ap user.subscriptions[0].forks
